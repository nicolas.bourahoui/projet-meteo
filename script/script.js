// Coordonnées des quartiers de Toulouse
var quartiers = {
  nord: {
    name: "Quartier Nord",
    coordinates: [
      [43.653149, 1.432050], // Coin supérieur gauche
      [43.637887, 1.481728], // Coin supérieur droit
      [43.602611, 1.486802], // Coin inférieur droit
      [43.602621, 1.451753], // Coin inférieur gauche bas
      [43.610259, 1.443808], // Coin inférieur gauche milieu
      [43.611453, 1.438898]  // Coin inférieur gauche haut
    ]
  },
  sud: {
    name: "Quartier Sud",
    coordinates: [
      [43.600118, 1.384309], // Coin supérieur gauche
      [43.601518, 1.428056], // Coin supérieur droit haut
      [43.592947, 1.434427], // Coin supérieur droit milieu
      [43.592142, 1.441345], // Coin supérieur droit bas
      [43.556007, 1.442783], // Coin inférieur droit
      [43.569843, 1.373583]  // Coin inférieur gauche
    ]
  },
  est: {
    name: "Quartier Est",
    coordinates: [
      [43.592142, 1.441345], // Coin supérieur gauche bas
      [43.595833, 1.452456], // Coin supérieur gauche milieu
      [43.602621, 1.451753], // Coin supérieur gauche haut
      [43.602611, 1.486802], // Coin supérieur droit
      [43.560642, 1.497655], // Coin inférieur droit
      [43.556007, 1.442783]  // Coin inférieur gauche
    ]
  },
  ouest: {
    name: "Quartier Ouest",
    coordinates: [
      [43.649535, 1.365724], // Coin supérieur gauche
      [43.653149, 1.432050], // Coin supérieur droit
      [43.611453, 1.438898], // Coin inférieur droit haut
      [43.608269, 1.428109], // Coin inférieur droit milieu
      [43.601518, 1.428056], // Coin inférieur droit bas
      [43.600118, 1.384309]  // Coin inférieur gauche
    ]
  }
};

// Initialisation de la cartes
var map = L.map('map').setView([43.604652, 1.444209], 13);

// Ajout de la couche de tuiles OpenStreetMap
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
  maxZoom: 18,
}).addTo(map);

// Ajout des polygones de délimitation pour chaque quartier
for (var quartier in quartiers) {
  var polygon = L.polygon(quartiers[quartier].coordinates, {color: 'red'}).addTo(map);
  polygon.bindPopup(quartiers[quartier].name);
}

// Écouteur d'événement pour le bouton de mise à jour
document.getElementById('update-btn').addEventListener('click', updateData);

// Fonction de mise à jour des données
function updateData() {
  var selectedMonth = document.getElementById('month').value;
  var selectedYear = document.getElementById('year').value;
  
  // Ici on réculère les données que l'on a dans l'ontologie
  // en fonction du mois et de l'année sélectionnés
  var temperature = 17 ; // Récupérer la température
  var tauxPluie = 0 ; // Récupérer le taux de pluie
  var humidite = 25 ; // Récupérer l'humidité
  var vitesseVent = 6 ; // Récupérer la vitesse du vent
  [temperature, tauxPluie, humidite, vitesseVent] = fetchFusekiData(selectedMonth, selectedYear)
  // Coordonnées des points où afficher le texte
  var pointNE = [43.624114, 1.461092];
  var pointNO = [43.626174, 1.408046];
  var pointSE = [43.582289, 1.465531];
  var pointSO = [43.584023, 1.409187];
  // Texte à afficher dans le cadre blanc
  var textContent = "Température : " + temperature + "°C<br> Taux de pluie : " + tauxPluie + "mm<br> Humidité : " + humidite + "%<br> Vitesse du vent : " + vitesseVent +" km/h";

  // Création de l'infobulle personnalisée
  var customNE = L.tooltip({
    className: 'map-tooltip',
    permanent: true,
    direction: 'center',
    offset: [0, -20],
    opacity: 1
  }).setContent('<div class="tooltip-content">' + textContent + '</div>');
  var customNO = L.tooltip({
    className: 'map-tooltip',
    permanent: true,
    direction: 'center',
    offset: [0, -20],
    opacity: 1
  }).setContent('<div class="tooltip-content">' + textContent + '</div>');
  var customSE = L.tooltip({
    className: 'map-tooltip',
    permanent: true,
    direction: 'center',
    offset: [0, -20],
    opacity: 1
  }).setContent('<div class="tooltip-content">' + textContent + '</div>');
  var customSO = L.tooltip({
    className: 'map-tooltip',
    permanent: true,
    direction: 'center',
    offset: [0, -20],
    opacity: 1
  }).setContent('<div class="tooltip-content">' + textContent + '</div>');

  // Positionnement des infobulles sur la carte
  L.marker(pointNE)
    .bindTooltip(customNE)
    .addTo(map);
  L.marker(pointNO)
    .bindTooltip(customNO)
    .addTo(map);
   L.marker(pointSE)
    .bindTooltip(customSE)
    .addTo(map);
   L.marker(pointSO)
    .bindTooltip(customSO)
    .addTo(map);
}


async function fetchFusekiData(mois,annee) {
  const fusekiUrl = "<http://www.semanticweb.org/adminetu/ontologies/meteo>"
  // La requête a exécuter
  const sparqlQuery = `
    PREFIX myont: <http://www.semanticweb.org/adminetu/ontologies/meteo>

      SELECT ?temperature ?pluie ?humidite ?Vent
      WHERE {
        ?quartier a-pour-taux-pluie  myont:pluie ?pluie ;
                  a-pour-temperature  myont:temperature ?temperature ;
                  a-pour-vitesse-de-vent  myont:Vent ?Vent ;
                  a-pour-humidute  myont:humidite ?humidite
        FILTER (?mois = "${mois}" && ?annee = "${annee}" && ?quartier = "blagnac")
      }
      LIMIT 1
  `;
  
  // Envoi de la requête SPARQL au serveur Fuseki
  const response = await fetch(fusekiUrl + '?query=' + encodeURIComponent(sparqlQuery) + '&format=json');
  const data = await response.json();
  var temp1;
  var pluie1;
  var humidite1;
  var vent1;

  // Récupération des variables du SELECT
  const bindings = data.results.bindings;
  if (bindings.length > 0) {
    temp1 = bindings[0].temperature.value;
    pluie1 = bindings[0].pluie.value;
    humidite1 = bindings[0].humidite.value;
    vent1 = bindings[0].Vent.value; 
  } else {
    temp1 = "Pas de données";
    pluie1 = "Pas de données";
    humidite1 = "Pas de données";
    vent1 = "Pas de données"; 
  }

  return [temp1, pluie1, humidite1, vent1]
}
