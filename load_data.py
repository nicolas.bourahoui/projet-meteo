import pandas as pd
import pandas as pd2
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import FOAF

# Lire le fichier CSV avec pandas
quartier_blagnac = pd.read_csv('data/blagnac.csv')
quartier_lardenne = pd2.read_csv('data/lardenne.csv')
#quartier_Paul_Sab = pd.read_csv('../data/paul_sab.csv')
#quartier_Parc_maourine = pd.read_csv('../data/parc_maourine.csv')

tab_quartiers = ([quartier_blagnac, 'blagnac'], [quartier_lardenne,'lardenne'])

# Créer un graphe RDF vide
graph = Graph()

# Définir les espaces de noms
MYNS = Namespace('http://www.semanticweb.org/adminetu/ontologies/meteo')
graph.bind('myont', MYNS)
graph.bind('foaf', FOAF)

#Parcours des 4 quartiers et création d'un fichier pour chaque quartiers 
for j in range(0, len(tab_quartiers)):
    # Parcourir les lignes du DataFrame
    for index, row in tab_quartiers[j][0].iterrows():
        # Créer un URI unique pour chaque ligne
        uri = MYNS[f"individual_{index + 1}"]

        # Ajouter les triplets RDF pour chaque colonne du CSV
        for colonne_name, value in row.items():
            tab_colname = colonne_name.split(';')
            tab_valeur = value.split(';')
            for i in range(0, len(tab_colname)):
                if (tab_valeur[19] == "2021" or tab_valeur[19] == "2022") and (tab_valeur[2] == "1" or tab_valeur[2] == "8"):

                    # Ajouter le triplet RDF pour définir le type de l'individu
                    graph.add((uri, RDF.type, MYNS.Meteo))
                    # Ajout de la constante de Quartier
                    graph.add((uri, MYNS["quartier"], Literal(tab_quartiers[j][1])))
                    if tab_colname[i] == "temperature_partie_entiere" :
                        property_name = MYNS["temperature"]  # Utiliser le nom de colonne comme propriété
                        graph.add((uri, property_name, Literal(tab_valeur[i])))
                    elif tab_colname[i] == "humidite" :
                        property_name = MYNS["humidite"]  # Utiliser le nom de colonne comme propriété
                        graph.add((uri, property_name, Literal(tab_valeur[i])))
                    elif tab_colname[i] == "pluie_intensite_max" :
                        property_name = MYNS["pluie"]  # Utiliser le nom de colonne comme propriété
                        graph.add((uri, property_name, Literal(tab_valeur[i])))
                    elif tab_colname[i] == "force_rafale_max" :
                        property_name = MYNS["Vent"]  # Utiliser le nom de colonne comme propriété
                        graph.add((uri, property_name, Literal(tab_valeur[i])))
                    elif tab_colname[i] == "annee_reelle" :
                        property_name = MYNS["annee"]  # Utiliser le nom de colonne comme propriété
                        graph.add((uri, property_name, Literal(tab_valeur[i])))
                    elif tab_colname[i] == "mois" :
                        property_name = MYNS["mois"]  # Utiliser le nom de colonne comme propriété
                        graph.add((uri, property_name, Literal(tab_valeur[i])))

# Écrire le graphe RDF dans un fichier Turtle
graph.serialize(destination='quartiers.ttl', format='turtle')